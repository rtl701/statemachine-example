

# Statemachine example

### What is a state machine?
A process of defining a number of states e.g. draft, created etc.. and storing the state at a given point in time. The state may be changed by inputs or actions.

## What code is in this repo?

This repo consists of a .NET Core web API and a couple of .NET standard class libraries, which provides access to manipulate an object's state. The state's are defined using the [Stateless](https://github.com/dotnet-state-machine/stateless "Stateless") NuGet package.

## Why have I written this?
I stumbled across the [stateless](https://github.com/dotnet-state-machine/stateless "Stateless") NuGet package and wanted to see what it was like. It immediately reminded me of a previous project that would have benefited from its use. It was an issue/case management issue, where a user could start a draft case, discard it if the draft was no longer valid. Create it to track the issue, close it if it was resolved. Then re-open it if they needed to add more detail. 

We effectively hand crafted our own and it was not as elegant or easily configurable. 

Once I'd written a quick example of using this NuGet package, the repo then became more about an opportunity to demonstrate writing clean testable code with high unit test coverage.

## Statemachine rules
The states and flow of the states can be seen clearly below:

![state rules](images/states.png)

The rules are configured in case.cs as per the snippet below:

```cs
    StateMode.Configure(State.Draft)
    .Permit(StateTrigger.Create, State.Created)
    .Permit(StateTrigger.Discard, State.Discarded);
    
    StateMode.Configure(State.Created)
    .Permit(StateTrigger.Close, State.Closed);
    
    StateMode.Configure(State.Closed)
    .Permit(StateTrigger.Open, State.Created);

```

## API usage

- Run the repo locally and it will fire up the swagger endpoint so you can easily try out the API
- The first endpoint you should call is start which will return an id
- The id can then be used in other methods like create, discard or get

## Unit testing

The repo is thoroughly tested, each project has tests and there's a set of integration tests too. NCrunch calculates the coverage is currently at 95.25%, an analysis of which can be seen below:

![code coverage](images/code-coverage.PNG)

## Requirements
- Requires .NET Core 3.1