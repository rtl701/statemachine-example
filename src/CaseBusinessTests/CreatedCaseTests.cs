﻿using CaseDomain.Enums;
using System;
using System.Linq;
using Xunit;

namespace CaseBusinessTests
{
    public class CreatedCaseTests : CaseBusinessTestBase
    {
        private readonly Guid _caseId;

        public CreatedCaseTests()
        {
            _caseId = Sut.StartCase();

            Sut.CreateCase(_caseId);
        }

        [Fact]
        public void Created_Case_Returns_Created_State()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            // Assert.
            Assert.Equal(State.Created, result.State);
        }

        [Fact]
        public void Created_Case_Can_Only_Close()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var permittedTriggers = result.GetPermittedTriggers();

            // Assert.
            Assert.Single(permittedTriggers);
            Assert.Equal(StateTrigger.Close, permittedTriggers.First());
        }

        [Fact]
        public void Created_Case_Can_Not_Discard()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var permittedTriggers = result.GetPermittedTriggers();

            var match = permittedTriggers.Where(p => p.Equals(StateTrigger.Discard));

            Assert.Empty(match);
        }

        [Fact]
        public void Created_Case_Discard_Throws()
        {
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.DiscardCase(_caseId));
        }
    }
}
