﻿using CaseDomain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseBusinessTests
{
    public class DiscardedCaseTests : CaseBusinessTestBase
    {
        private readonly Guid _caseId;

        public DiscardedCaseTests()
        {
            _caseId = Sut.StartCase();

            Sut.DiscardCase(_caseId);
        }

        [Fact]
        public void DicardedCase_Returns_Expected_State()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            // Assert.
            Assert.Equal(State.Discarded, result.State);
        }

        [Fact]
        public void DiscardedCase_No_Permitted_Triggers()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var permittedTriggers = result.GetPermittedTriggers();

            // Assert.
            Assert.Empty(permittedTriggers);
        }

        [Fact]
        public void DiscardedCase_CreateCase_Throws()
        { 
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.CreateCase(_caseId));
        }
    }
}
