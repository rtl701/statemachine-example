﻿using CaseBusiness;
using CaseDomain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CaseBusinessTests
{
    public class DraftCaseTests : CaseBusinessTestBase
    {
        [Fact]
        public void GetCaseState_DraftState_Returns_Expected_String()
        {
            // Arrange.
            var draftCaseId = Sut.StartCase();

            // Act.
            var result = Sut.GetCaseState(draftCaseId);

            // Assert.
            Assert.Equal("StateMachine { State = Draft, PermittedTriggers = { Create, Discard }}", result);
        }

        [Fact]
        public void GetCaseStateObject_DraftCase_Returns_Draft_State()
        {
            // Arrange.
            var draftCaseId = Sut.StartCase();

            // Act.
            var result = Sut.GetCaseStateObject(draftCaseId);

            // Assert.
            Assert.Equal(State.Draft, result.State);
        }

        [Theory]
        [InlineData(StateTrigger.Create)]
        [InlineData(StateTrigger.Discard)]
        public void GetCaseStateObject_DraftCase_Returns_ExpectedTriggers(StateTrigger stateTrigger)
        {
            // Arrange.
            var draftCaseId = Sut.StartCase();

            // Act.
            var result = Sut.GetCaseStateObject(draftCaseId);

            var triggerMatch = result.PermittedTriggers.ToList().Where(p => p.Equals(stateTrigger));

            // Assert.
            Assert.NotEmpty(triggerMatch);
        }

        [Theory]
        [InlineData(StateTrigger.Close)]
        [InlineData(StateTrigger.Open)]
        public void GetCaseStateObject_DraftCase_DoesNot_Return_UnExpectedTriggers(StateTrigger stateTrigger)
        {
            // Arrange.
            var draftCaseId = Sut.StartCase();

            // Act.
            var result = Sut.GetCaseStateObject(draftCaseId);

            var triggerMatch = result.PermittedTriggers.ToList().Where(p => p.Equals(stateTrigger));

            // Assert.
            Assert.Empty(triggerMatch);
        }
    }
}
