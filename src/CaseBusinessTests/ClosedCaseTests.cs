﻿using CaseDomain.Enums;
using System;
using System.Linq;
using Xunit;

namespace CaseBusinessTests
{
    public class ClosedCaseTests : CaseBusinessTestBase
    {
        private readonly Guid _caseId;
        public ClosedCaseTests()
        {
            _caseId = Sut.StartCase();

            Sut.CreateCase(_caseId);
            Sut.CloseCase(_caseId);
        }

        [Fact]
        public void ClosedCase_Returns_Expected_State()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            // Assert.
            Assert.Equal(State.Closed, result.State);
        }

        [Fact]
        public void ClosedCase_Can_Only_Trigger_Open()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var permittedTriggers = result.GetPermittedTriggers();

            // Assert.
            Assert.Single(permittedTriggers);

            Assert.Equal(StateTrigger.Open, permittedTriggers.First());
        }

        [Fact]
        public void ClosedCase_DiscardCase_Throws()
        {
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.DiscardCase(_caseId));
        }

        [Fact]
        public void ClosedCase_CreateCase_Throws()
        {
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.CreateCase(_caseId));
        }
    }
}
