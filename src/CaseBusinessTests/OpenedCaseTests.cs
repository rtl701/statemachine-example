﻿using CaseDomain.Enums;
using System;
using System.Linq;
using Xunit;

namespace CaseBusinessTests
{
    public class OpenedCaseTests : CaseBusinessTestBase
    {
        private readonly Guid _caseId;

        public OpenedCaseTests()
        {
            _caseId = Sut.StartCase();
            Sut.CreateCase(_caseId);
            Sut.CloseCase(_caseId);
            Sut.OpenCase(_caseId);
        }

        [Fact]
        public void OpenedCase_Can_Close()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var permittedTriggers = result.GetPermittedTriggers();

            // Assert.
            Assert.Single(permittedTriggers);
            Assert.Equal(StateTrigger.Close, permittedTriggers.FirstOrDefault());
        }

        [Fact]
        public void OpenedCase_Returns_Expected_State()
        {
            // Act.
            var result = Sut.GetCaseStateObject(_caseId);

            var state = result.State;

            // Assert.
            Assert.Equal(State.Created, state);
        }

        [Fact]
        public void OpenedCase_DiscardCase_Throws()
        {
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.DiscardCase(_caseId));
        }

        [Fact]
        public void OpenedCase_CreateCase_Throws()
        {
            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => Sut.CreateCase(_caseId));
        }
    }
}
