using System;
using Xunit;

namespace CaseBusinessTests
{
    public class CasesBusinessTests : CaseBusinessTestBase
    {
        

        [Fact]
        public void StartCase_Returns_Guid()
        {
            // Act.
            var result = Sut.StartCase();

            // Assert.
            Assert.IsType<Guid>(result);
        }

        [Fact]
        public void GetCaseState_Invalid_Guid_Throws()
        {
            // Act/Assert.
            var result = Assert.Throws<InvalidOperationException>(() => Sut.GetCaseState(Guid.NewGuid()));
        }
    }
}
