﻿using CaseApi.Controllers;
using CaseBusiness;
using Moq;

namespace CaseApiTests.CaseControllerTests
{
    public abstract class CaseControllerTestBase
    {
        protected readonly Mock<ICaseBusiness> MockCaseBusiness = new Mock<ICaseBusiness>();
        protected readonly CaseController Sut;

        public CaseControllerTestBase()
        {
            Sut = new CaseController(MockCaseBusiness.Object);
        }
    }
}
