﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class GetTests : CaseControllerTestBase
    {
        [Fact]
        public void Get_Returns_OkObjectResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            MockCaseBusiness.Setup(m => m.GetCaseState(caseId)).Returns("Test");

            // Act.
            var result = Sut.Get(caseId);

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_Returns_ExpectedResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            MockCaseBusiness.Setup(m => m.GetCaseState(caseId)).Returns("Test");

            // Act.
            var result = (OkObjectResult)Sut.Get(caseId);

            // Assert.
            Assert.Equal("Test", result.Value);
        }

        [Fact]
        public void Get_Calls_CaseBusiness_GetCaseState_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            MockCaseBusiness.Setup(m => m.GetCaseState(caseId)).Returns("Test");

            // Act.
            var result = (OkObjectResult)Sut.Get(caseId);

            // Assert.
            MockCaseBusiness.Verify(m => m.GetCaseState(caseId), Times.Once);
        }
    }
}
