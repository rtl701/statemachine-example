﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class CreateTests : CaseControllerTestBase
    {
        [Fact]
        public void Create_Calls_CaseBusiness_Times_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            // Act.
            var result = Sut.Create(caseId);

            // Assert.
            MockCaseBusiness.Verify(m => m.CreateCase(caseId), Times.Once);
        }

        [Fact]
        public void Create_Returns_OkObjectResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            // Act.
            var result = Sut.Create(caseId);

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Create_Returns_Expected_Value()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            var returnedString = "Test create";

            MockCaseBusiness.Setup(m => m.CreateCase(caseId)).Returns(returnedString);

            // Act.
            var result = (OkObjectResult)Sut.Create(caseId);

            // Assert.
            Assert.Equal(returnedString, result.Value);
        }
    }
}
