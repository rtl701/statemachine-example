﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class CloseTests : CaseControllerTestBase
    {
        [Fact]
        public void Close_Calls_CaseBusiness_CloseCase_Times_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            // Act.
            Sut.Close(caseId);

            // Assert.
            MockCaseBusiness.Verify(m => m.CloseCase(caseId), Times.Once);
        }

        [Fact]
        public void Close_Returns_OkObjectResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            // Act.
            var result = Sut.Close(caseId);

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Close_Returns_Expected_Value()
        {
            // Arrange.
            var caseId = Guid.NewGuid();
            var returnString = "Close test";

            MockCaseBusiness.Setup(m => m.CloseCase(caseId)).Returns(returnString);

            // Act.
            var result = (OkObjectResult)Sut.Close(caseId);

            // Assert.
            Assert.Equal(returnString, result.Value);
        }
    }
}
