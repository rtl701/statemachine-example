﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class DiscardTests : CaseControllerTestBase
    {
        [Fact]
        public void Discard_Calls_CaseBusiness_DiscardCase_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            // Act.
            var result = Sut.Discard(caseId);

            // Assert.
            MockCaseBusiness.Verify(m => m.DiscardCase(caseId), Times.Once);
        }

        [Fact]
        public void Discard_Returns_OkObjectResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();
            var returnString = "Test discard";
            MockCaseBusiness.Setup(m => m.DiscardCase(caseId)).Returns(returnString);

            // Act.
            var result = Sut.Discard(caseId);

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Discard_Returns_Expected_Value()
        {
            // Arrange.
            var caseId = Guid.NewGuid();
            var returnString = "Test discard";
            MockCaseBusiness.Setup(m => m.DiscardCase(caseId)).Returns(returnString);

            // Act.
            var result = (OkObjectResult)Sut.Discard(caseId);

            // Assert.
            Assert.Equal(returnString, result.Value);
        }
    }
}
