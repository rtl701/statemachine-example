﻿using CaseDomain.Enums;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Stateless;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class GetStateObjectTests : CaseControllerTestBase
    {
        [Fact]
        public void GetStateObject_Returns_OkObjectResult()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            var stateMachine = new StateMachine<State, StateTrigger>(State.Draft);

            MockCaseBusiness.Setup(m => m.GetCaseStateObject(caseId)).Returns(stateMachine);

            // Act.
            var result = Sut.GetStateObject(caseId);

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetStateObject_Returns_Expected_Value()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            var stateMachine = new StateMachine<State, StateTrigger>(State.Draft);

            MockCaseBusiness.Setup(m => m.GetCaseStateObject(caseId)).Returns(stateMachine);

            // Act.
            var result = (OkObjectResult)Sut.GetStateObject(caseId);

            // Assert.
            Assert.Equal(stateMachine, result.Value);
        }

        [Fact]
        public void GetStateObject_Calls_CaseBusiness_GetCaseStateObject_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();

            var stateMachine = new StateMachine<State, StateTrigger>(State.Draft);

            MockCaseBusiness.Setup(m => m.GetCaseStateObject(caseId)).Returns(stateMachine);

            // Act.
            var result = Sut.GetStateObject(caseId);

            // Assert.
            MockCaseBusiness.Verify(m => m.GetCaseStateObject(caseId), Times.Once);
        }
    }
}
