﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseApiTests.CaseControllerTests
{
    public class StartTests : CaseControllerTestBase
    {
        [Fact]
        public void Start_Returns_OkObjectResult()
        {
            // Act.
            var result = Sut.Start();

            // Assert.
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Start_Returns_Expected_Result()
        {
            // Arrange.
            var caseId = Guid.NewGuid();
            MockCaseBusiness.Setup(m => m.StartCase()).Returns(caseId);

            // Act.
            var result = Sut.Start();

            var okResult = (OkObjectResult)result;

            // Assert.
            Assert.Equal(caseId, okResult.Value);
        }

        [Fact]
        public void Start_Calls_CaseBusiness_StartCase_Once()
        {
            // Arrange.
            var caseId = Guid.NewGuid();
            MockCaseBusiness.Setup(m => m.StartCase()).Returns(caseId);

            // Act.
            var result = Sut.Start();

            // Assert.
            MockCaseBusiness.Verify(m => m.StartCase(), Times.Once);
        }
    }
}
