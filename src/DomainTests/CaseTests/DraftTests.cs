﻿using CaseDomain;
using CaseDomain.Enums;
using System.Linq;
using Xunit;

namespace CaseDomainTests.CaseTests
{
    public class DraftTests
    {
        [Fact]
        public void Constructor_Sets_To_Draft_State()
        {
            // Act.
            var testCase = new Case();

            // Assert.
            Assert.Equal(State.Draft, testCase.StateMode.State);
        }

        [Theory]
        [InlineData(StateTrigger.Create)]
        [InlineData(StateTrigger.Discard)]
        public void Draft_Can_Perform_Expected_Trigger(StateTrigger stateTrigger)
        {
            // Act.
            var testCase = new Case();

            var permittedTrigger = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(stateTrigger));

            //
            Assert.NotEmpty(permittedTrigger);
        }

        [Theory]
        [InlineData(StateTrigger.Close)]
        [InlineData(StateTrigger.Open)]
        public void Draft_Can_Not_Perform_Expected_Trigger(StateTrigger stateTrigger)
        {
            // Act.
            var testCase = new Case();

            var permittedTrigger = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(stateTrigger));

            //
            Assert.Empty(permittedTrigger);
        }
    }
}
