﻿using CaseDomain;
using CaseDomain.Enums;
using System;
using System.Linq;
using Xunit;

namespace CaseDomainTests.CaseTests
{
    public class CreateTests
    {
        [Fact]
        public void Created_One_PermittedTrigger()
        {
            // Arrange.
            var testCase = new Case();

            // Arrange.
            testCase.CreateCase();

            var permittedTriggers = testCase.StateMode.GetPermittedTriggers();

            // Assert.
            Assert.Single(permittedTriggers);
        }

        [Fact]
        public void Created_Can_Close()
        {
            // Arrange.
            var testCase = new Case();

            // Arrange.
            testCase.CreateCase();

            var permittedTriggers = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(StateTrigger.Close));

            // Assert.
            Assert.Single(permittedTriggers);
        }

        [Theory]
        [InlineData(StateTrigger.Create)]
        [InlineData(StateTrigger.Discard)]
        [InlineData(StateTrigger.Open)]
        public void Created_Can_Not_Perform_Unexpected_Trigger(StateTrigger stateTrigger)
        {
            // Arrange.
            var testCase = new Case();

            // Arrange.
            testCase.CreateCase();

            var permittedTriggers = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(stateTrigger));

            // Assert.
            Assert.Empty(permittedTriggers);
        }

        [Fact]
        public void Created_Sets_Created_State()
        {
            // Arrange.
            var testCase = new Case();

            // Arrange.
            testCase.CreateCase();

            // Assert.
            Assert.Equal(State.Created, testCase.StateMode.State);
        }

        [Fact]
        public void Create_Discarded_Case_Throws()
        {
            // Arrange.
            var testCase = new Case();
            testCase.DiscardCase();

            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => testCase.CreateCase());
        }

        [Fact]
        public void Create_Closed_Case_Throws()
        {
            // Arrange.
            var testCase = new Case();
            testCase.CreateCase();
            testCase.CloseCase();

            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => testCase.CreateCase());
        }
    }
}
