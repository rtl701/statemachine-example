﻿using CaseDomain;
using CaseDomain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseDomainTests.CaseTests
{
    public class DiscardTests
    {
        [Fact]
        public void Discard_Returns_No_PermittedTriggers()
        {
            // Arrange.
            var testCase = new Case();

            // Act.
            testCase.DiscardCase();

            var permittedTriggers = testCase.StateMode.GetPermittedTriggers();

            // Assert.
            Assert.Empty(permittedTriggers);
        }

        [Fact]
        public void Discard_Returns_Discarded_State()
        {
            // Arrange.
            var testCase = new Case();

            // Act.
            testCase.DiscardCase();

            // Assert.
            Assert.Equal(State.Discarded, testCase.StateMode.State);
        }

        [Fact]
        public void Discard_Created_Case_Throws()
        {
            // Arrange.
            var testCase = new Case();
            testCase.CreateCase();

            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => testCase.DiscardCase());
        }

        [Fact]
        public void Discard_Closed_Case_Throws()
        {
            // Arrange.
            var testCase = new Case();
            testCase.CreateCase();
            testCase.CloseCase();

            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => testCase.DiscardCase());
        }
    }
}
