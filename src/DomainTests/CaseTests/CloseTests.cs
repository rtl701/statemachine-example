﻿using CaseDomain;
using CaseDomain.Enums;
using System;
using Xunit;

namespace CaseDomainTests.CaseTests
{
    public class CloseTests
    {
        [Fact]
        public void Close_Can_Only_One_PermittedTrigger()
        {
            // Arrange/Act.
            var closedCase = SetupClosedCase();

            // Assert.
            Assert.Single(closedCase.StateMode.GetPermittedTriggers());
        }

        [Fact]
        public void Close_Sets_Closed_State()
        {
            // Arrange/Act.
            var closedCase = SetupClosedCase();

            // Assert.
            Assert.Equal(State.Closed, closedCase.StateMode.State);
        }

        [Fact]
        public void Close_Case_Discard_Throws()
        {
            // Arrange.
            var closedCase = SetupClosedCase();

            // Act/Assert.
            Assert.Throws<InvalidOperationException>(() => closedCase.DiscardCase());
        }

        private Case SetupClosedCase()
        {
            var testCase = new Case();

            testCase.CreateCase();

            testCase.CloseCase();

            return testCase;
        }
    }
}
