﻿using CaseDomain;
using CaseDomain.Enums;
using System.Linq;
using Xunit;

namespace CaseDomainTests.CaseTests
{
    public class StartTests
    {
        [Fact]
        public void Start_Sets_State_Draft()
        {
            // Act.
            var testCase = new Case();

            // Assert.
            Assert.Equal(State.Draft, testCase.StateMode.State);
        }

        [Theory]
        [InlineData(StateTrigger.Create)]
        [InlineData(StateTrigger.Discard)]
        public void Start_Returns_Expected_PermittedTriggers(StateTrigger stateTrigger)
        {
            // Act.
            var testCase = new Case();

            var stateTriggerMatch = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(stateTrigger));
            // Assert.
            Assert.NotEmpty(stateTriggerMatch);
        }

        [Theory]
        [InlineData(StateTrigger.Close)]
        [InlineData(StateTrigger.Open)]
        public void Start_Does_Not_Return_Unexpected_PermittedTriggers(StateTrigger stateTrigger)
        {
            // Act.
            var testCase = new Case();

            var stateTriggerMatch = testCase.StateMode.GetPermittedTriggers().Where(p => p.Equals(stateTrigger));
            // Assert.
            Assert.Empty(stateTriggerMatch);
        }
    }
}
