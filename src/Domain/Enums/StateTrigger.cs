﻿
namespace CaseDomain.Enums
{
    public enum StateTrigger
    {
        Create,
        Close,
        Open,
        Discard
    }
}
