﻿
namespace CaseDomain.Enums
{
    public enum State
    {
        Draft = 0,
        Created = 1,
        Closed = 2,
        Discarded = 3
    }
}
