﻿using CaseDomain.Enums;
using Stateless;
using System;

namespace CaseDomain
{
    public class Case
    {
        public StateMachine<State, StateTrigger> StateMode { get; private set; }
        public Guid Id { get; private set; }

        public Case()
        {
            StateMode = new StateMachine<State, StateTrigger>(State.Draft);

            ConfigureStates();
            
            Id = Guid.NewGuid();
        }

        private void ConfigureStates()
        {
            StateMode.Configure(State.Draft)
                .Permit(StateTrigger.Create, State.Created)
                .Permit(StateTrigger.Discard, State.Discarded);

            StateMode.Configure(State.Created)
                .Permit(StateTrigger.Close, State.Closed);

            StateMode.Configure(State.Closed)
                .Permit(StateTrigger.Open, State.Created);
        }

        public void CreateCase()
        {
            StateMode.Fire(StateTrigger.Create);
        }

        public void DiscardCase()
        {
            StateMode.Fire(StateTrigger.Discard);
        }

        public void OpenCase()
        {
            StateMode.Fire(StateTrigger.Open);
        }

        public void CloseCase()
        {
            StateMode.Fire(StateTrigger.Close);
        }
    }
}
