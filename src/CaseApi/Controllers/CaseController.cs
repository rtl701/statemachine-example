﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CaseBusiness;

namespace CaseApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseController : ControllerBase
    {
        private readonly ICaseBusiness _caseBusiness;
        private readonly string _apiViolationMessage = "API violation.";

        public CaseController(ICaseBusiness caseBusiness)
        {
            _caseBusiness = caseBusiness;
        }

        /// <summary>
        /// Starts a case.
        /// </summary>
        /// <returns>Returns a Guid of the caseId.</returns>
        [HttpGet]
        [Route("Start")]
        public IActionResult Start()
        {
            return Ok(_caseBusiness.StartCase());
        }

        /// <summary>
        /// Gets a string representation of the current state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>State machine string.</returns>
        // GET: api/Case/xxxx-...
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.GetCaseState(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }
        }

        /// <summary>
        /// Gets a statemachine object of the current state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>State machine object.</returns>
        // GET: api/Case/StateObject/xxxx...
        [HttpGet]
        [Route("StateObject/{id}")]
        public IActionResult GetStateObject(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.GetCaseStateObject(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }
        }

        /// <summary>
        /// Sets a case to a discarded state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>A state machine string.</returns>
        // GET: api/Case/Discard/xxxx-...
        [HttpPut]
        [Route("Discard/{id}")]
        public IActionResult Discard(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.DiscardCase(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }
  
        }

        /// <summary>
        /// Sets a case to a created state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>A state machine string.</returns>
        // Put: api/Case/Create/xxxx-...
        [HttpPut]
        [Route("Create/{id}")]
        public IActionResult Create(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.CreateCase(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }

        }

        /// <summary>
        /// Sets a case to an open state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>A state machine string.</returns>
        // GET: api/Case/Open/xxxx-...
        [HttpPut]
        [Route("Open/{id}")]
        public IActionResult Open(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.OpenCase(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }
        }

        /// <summary>
        /// Sets the case to a closed state.
        /// </summary>
        /// <param name="id">Case id.</param>
        /// <returns>A state machine string.</returns>
        // GET: api/Case/Close/xxxx-...
        [HttpPut]
        [Route("Close/{id}")]
        public IActionResult Close(Guid id)
        {
            try
            {
                return Ok(_caseBusiness.CloseCase(id));
            }
            catch (InvalidOperationException ex)
            {
                return Ok(GetApiViolationMessage(ex.Message));
            }
        }

        private string GetApiViolationMessage(string exceptionMessage) 
        {
            return $"{_apiViolationMessage} {exceptionMessage}";
        }
    }
}
