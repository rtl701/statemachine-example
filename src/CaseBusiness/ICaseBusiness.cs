﻿using CaseDomain.Enums;
using Stateless;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseBusiness
{
    public interface ICaseBusiness
    {
        Guid StartCase();

        string DiscardCase(Guid id);

        string CreateCase(Guid id);

        string CloseCase(Guid id);

        string OpenCase(Guid id);

        string GetCaseState(Guid id);

        StateMachine<State, StateTrigger> GetCaseStateObject(Guid id);
    }
}
