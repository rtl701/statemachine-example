﻿using CaseDomain;
using CaseDomain.Enums;
using Stateless;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CaseBusiness
{
    public class CasesBusiness : ICaseBusiness
    {
        private List<Case> Cases { get; set; }

        public CasesBusiness()
        {
            Cases = new List<Case>();
        }

        public string CloseCase(Guid id)
        {
            var caseToClose = GetCase(id);

            caseToClose.CloseCase();

            return caseToClose.StateMode.ToString();
        }

        public string CreateCase(Guid id)
        {
            var caseToCreate = GetCase(id);

            caseToCreate.CreateCase();

            return caseToCreate.StateMode.ToString();

        }

        public string DiscardCase(Guid id)
        {
            var caseToDiscard = GetCase(id);

            caseToDiscard.DiscardCase();

            return caseToDiscard.StateMode.ToString();
        }

        public string GetCaseState(Guid id)
        {
            var caseToGetStatus = GetCase(id);

            return caseToGetStatus.StateMode.ToString();
        }

        public StateMachine<State, StateTrigger> GetCaseStateObject(Guid id)
        {
            var caseToGetStatus = GetCase(id);

            return caseToGetStatus.StateMode;
        }

        public string OpenCase(Guid id)
        {
            var caseToOpen = GetCase(id);

            caseToOpen.OpenCase();

            return caseToOpen.StateMode.ToString();
        }

        public Guid StartCase()
        {
            var newCase = new Case();

            Cases.Add(newCase);

            return newCase.Id;
        }

        private Case GetCase(Guid id)
        { 
            var caseToGet = Cases.Single(c => c.Id.Equals(id));

            return caseToGet;
        }
    }
}
