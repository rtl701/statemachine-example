﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace CaseApiIntegrationTests.Helpers
{
    public static class ContentHelper
    {
        /// <summary>
        /// Gets an empty content string, use when there's no content and you are updating via the query string.
        /// </summary>
        /// <returns>Returns an empty content string</returns>
        public static StringContent GetEmptyStringContent()
        {
            return new StringContent(JsonConvert.SerializeObject(new { }), Encoding.Default, "application/json");
        }
    }
}
