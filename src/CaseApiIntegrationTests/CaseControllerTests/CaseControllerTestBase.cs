﻿using CaseApi;
using CaseApiIntegrationTests.Helpers;
using CaseBusiness;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public abstract class CaseControllerTestBase
    {
        protected readonly System.Net.Http.HttpClient HttpClient;
        protected const string BaseUrl = "api/case/";

        public CaseControllerTestBase()
        {
            var startup = new WebHostBuilder()
            .UseStartup<Startup>()
            .ConfigureServices(x =>
            {
                x.AddSingleton<ICaseBusiness>(new CasesBusiness());
            });
                    var testServer = new TestServer(startup);
                    HttpClient = testServer.CreateClient();
        }

        protected async Task<Guid> GetDraftCaseId()
        {
            // Start a case and get draft id.
            var draftQuery = $"{BaseUrl}start";

            var response = await HttpClient.GetAsync(draftQuery);

            var responseJson = await response.Content.ReadAsStringAsync();
            var responseObject = JsonConvert.DeserializeObject<Guid>(responseJson);

            return responseObject;
        }

        protected async Task CreateCase(Guid id)
        {
            var createQuery = $"{BaseUrl}create/{id}";

            await HttpClient.PutAsync(createQuery, ContentHelper.GetEmptyStringContent());
        }
    }
}
