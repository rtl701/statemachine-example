﻿using CaseApiIntegrationTests.Helpers;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class CloseTests : CaseControllerTestBase
    {
        [Fact]
        public async Task Close_Created_Case_Returns_Ok_StatusCode()
        {
            //  Arrange.
            var caseId = await GetDraftCaseId();
            await CreateCase(caseId);

            // Act.
            var uri = $"{BaseUrl}close/{caseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            // Assert.
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Close_Draft_Case_Returns_Ok_StatusCode()
        {
            //  Arrange.
            var caseId = await GetDraftCaseId();

            // Act/
            var uri = $"{BaseUrl}close/{caseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            // Assert.
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Close_Draft_Case_Returns_Api_Violation_String()
        {
            //  Arrange.
            var caseId = await GetDraftCaseId();

            // Act/
            var uri = $"{BaseUrl}close/{caseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());


            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("API violation. No valid leaving transitions are permitted from state 'Draft' for trigger 'Close'. Consider ignoring the trigger.", responseString);
        }

        [Fact]
        public async Task Close_Returns_ExpectedString()
        {
            // Arrange
            var caseId = await GetDraftCaseId();
            await CreateCase(caseId);
            var uri = $"{BaseUrl}close/{caseId}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("StateMachine { State = Closed, PermittedTriggers = { Open }}", responseString);
        }
    }
}
