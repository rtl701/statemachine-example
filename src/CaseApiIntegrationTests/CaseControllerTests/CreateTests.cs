﻿using CaseApiIntegrationTests.Helpers;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class CreateTests : CaseControllerTestBase
    {
        [Fact]
        public async Task Create_Invalid_Returns_Ok_Status_Code()
        {
            // Arrange.
            var uri = $"{BaseUrl}create/{Guid.NewGuid()}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create_Invalid_Contains_Api_Violation_string()
        {
            // Arrange.
            var uri = $"{BaseUrl}create/{Guid.NewGuid()}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("API violation. Sequence contains no matching element", responseString);
        }

        [Fact]
        public async Task Create_From_Draft_Returns_Expected_String()
        {
            // Arrange
            var caseId = await GetDraftCaseId();
            var uri = $"{BaseUrl}create/{caseId}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("StateMachine { State = Created, PermittedTriggers = { Close }}", responseString);
        }

        [Fact]
        public async Task Create_From_Draft_Returns_Ok_Response()
        {
            // Arrange
            var caseId = await GetDraftCaseId();
            var uri = $"{BaseUrl}create/{caseId}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            // Assert.
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
