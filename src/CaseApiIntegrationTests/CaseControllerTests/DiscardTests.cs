﻿using CaseApiIntegrationTests.Helpers;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class DiscardTests : CaseControllerTestBase
    {
        [Fact]
        public async Task Discard_Draft_Case_Returns_Ok_Status_Code()
        {
            // Arrange.
            var draftCaseId = await GetDraftCaseId();

            // Act.
            var uri = $"{BaseUrl}discard/{draftCaseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            // Assert.
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Discard_Created_Case_Returns_Ok_StatusCode()
        {
            //  Arrange.
            var caseId = await GetDraftCaseId();
            await CreateCase(caseId);

            // Act.
            var uri = $"{BaseUrl}discard/{caseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            // Assert.
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Discard_Created_Case_Handles_Exception()
        {
            //  Arrange.
            var caseId = await GetDraftCaseId();
            await CreateCase(caseId);

            // Act.
            var uri = $"{BaseUrl}discard/{caseId}";
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("API violation. No valid leaving transitions are permitted from state 'Created' for trigger 'Discard'. Consider ignoring the trigger.", responseString);
        }

        [Fact]
        public async Task Discard_Returns_ExpectedString()
        { 
            // Arrange
            var caseId = await GetDraftCaseId();
            var uri = $"{BaseUrl}discard/{caseId}";

            // Act.
            var response = await HttpClient.PutAsync(uri, ContentHelper.GetEmptyStringContent());

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("StateMachine { State = Discarded, PermittedTriggers = {  }}", responseString);
        }
    }
}
