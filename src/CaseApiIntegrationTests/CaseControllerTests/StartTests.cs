﻿using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class StartTests : CaseControllerTestBase
    {
        [Fact]
        public async Task Start_NoParameters_Returns_String()
        {
            // Arrange.
            var query = $"{BaseUrl}start";

            // Act.
            var response = await HttpClient.GetAsync(query);

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.NotEmpty(responseString);
        }

        [Fact]
        public async Task Start_Returns_200_Response()
        {
            // Arrange.
            var query = $"{BaseUrl}start";

            // Act.
            var response = await HttpClient.GetAsync(query);
            
            // Assert.
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
