﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class GetTests : CaseControllerTestBase
    {
        [Fact]
        public async Task Get_Invalid_Id_Returns_Ok_status_Code()
        {
            // Arrange.
            var uri = $"{BaseUrl}{Guid.NewGuid()}";
            
            // Act.
            var response = await HttpClient.GetAsync(uri);

            // Assert.
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_Invalid_Id_Returns_Api_Violation_string()
        {
            // Arrange.
            var uri = $"{BaseUrl}{Guid.NewGuid()}";

            // Act.
            var response = await HttpClient.GetAsync(uri);

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("API violation. Sequence contains no matching element", responseString);
        }

        [Fact]
        public async Task Get_Valid_Id_Returns_Expected_String()
        {
            // Arrange.
            var draftId = await GetDraftCaseId();
            var uri = $"{BaseUrl}{draftId}";


            // Act.
            var response = await HttpClient.GetAsync(uri);

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("StateMachine { State = Draft, PermittedTriggers = { Create, Discard }}", responseString);
        }

        [Fact]
        public async Task Get_Valid_Id_Returns_Ok_Status_Response()
        {
            // Arrange.
            var draftId = await GetDraftCaseId();
            var uri = $"{BaseUrl}{draftId}";
            
            // Act.
            var response = await HttpClient.GetAsync(uri);
            
            // Assert.
            response.EnsureSuccessStatusCode();
        }
    }
}
