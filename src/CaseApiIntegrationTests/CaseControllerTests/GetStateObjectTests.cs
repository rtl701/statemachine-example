﻿using CaseDomain.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stateless;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CaseApiIntegrationTests.CaseControllerTests
{
    public class GetStateObjectTests : CaseControllerTestBase
    {
        [Fact]
        public async Task GetStateObject_Draft_Case_Id_Returns_Expected_String()
        {
            // Arrange.
            var draftId = await GetDraftCaseId();
            var uri = $"{BaseUrl}stateObject/{draftId}";

            // Act.
            var response = await HttpClient.GetAsync(uri);
            var responseString = await response.Content.ReadAsStringAsync();
            
            // Assert.
            Assert.Equal(@"{""state"":0,""permittedTriggers"":[0,3]}", responseString);
        }

        [Fact]
        public async Task GetStateObject_Invalid_Draft_Case_Id_Returns_Ok_Status_Code()
        {
            // Arrange.
            var uri = $"{BaseUrl}stateObject/{Guid.NewGuid()}";

            // Act.
            var response = await HttpClient.GetAsync(uri);

            // Assert.
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetStateObject_Invalid_Draft_Case_Id_Returns_Api_Violation_String()
        {
            // Arrange.
            var uri = $"{BaseUrl}stateObject/{Guid.NewGuid()}";

            // Act.
            var response = await HttpClient.GetAsync(uri);
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert.
            Assert.Equal("API violation. Sequence contains no matching element", responseString);
        }
    }
}
